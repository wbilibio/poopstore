<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações
// com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'poopstore');


/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');


/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');


/** Nome do host do MySQL */
define('DB_HOST', 'localhost');


/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');


/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '~VqRW7$f)bb5,o=1SP$w0wP7Gue@Q|<->iKRj)ehYM:yI6,ZOvA}S@Q]SlfQIrJ.');

define('SECURE_AUTH_KEY',  'Z+jB[u*)1&whN[UQ0wkJ_C9.G4uk^0>@{U}4PZ^H;-R;pXtQWZs+d(WD[J.Vv*0U');

define('LOGGED_IN_KEY',    't@D9B}*ta_@kHNG6z04H~qBO[wcRmR?EHY.C2vtfkPlG,kv>HfL,@fRu]KKM2)x3');

define('NONCE_KEY',        'e1$<(X*8[EmFA%ENL}icqO~Q<QC>-ShR%SF1 r;f$[Z@VZ3bxrX95bedk}kn4z<3');

define('AUTH_SALT',        'Va!Q y9T=i<l]G5ppTAvq`k5/3q+Pc$(12+(_HI}a].O|fpK!E1y6}=qRof]s|lp');

define('SECURE_AUTH_SALT', '_|iUis(1EPW*PYA}2oG.[&]b!${y_-0&vFvBd+SR+(nDg)F6 .q)`eo|8dTe;8:=');

define('LOGGED_IN_SALT',   'CGM%z`DsE0qWKiD Pr2Qht5I1qQ*pa}Jq!?xtNHA$>}vBleO&Qp7xsec)?B!UDwu');

define('NONCE_SALT',       'mI&_R`FF=QX<:+v;3z2&xbWrFjea[veAujtu>dJQ8tygTwpfw/T^d!Vc&jQ,9 vd');


/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';


/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', true);

define('WP_MEMORY_LIMIT', '64M');

define ( 'AUTOMATIC_UPDATER_DISABLED' , true);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
