(function($){

    "use strict";

	function mainJS(){
		jQuery('.wpcf7-form').find('button[type="submit"]').click(function(){
			jQuery(this).parent().addClass('loader-submit');
		});
		if(jQuery('#wwlc_cf_cnpj').size() > 0) jQuery('#wwlc_cf_cnpj').mask('00.000.000/0000-00', {reverse: true});
		if(jQuery('input[name="billing_cpf"]').size() > 0) jQuery('input[name="billing_cpf"]').mask('999.999.999-99', {reverse: true});
		if(jQuery('#wwlc_phone').size() > 0) jQuery('#wwlc_phone').mask('(00) 0000-00000');


		$('#ie_field').find('.woocommerce-input-wrapper').prepend( "<label class='input-isento'><input type='checkbox' v-model='isento' />Isento</label>" );

	}

	mainJS();

})(jQuery);

window.onload = function ($) {
	if(jQuery('#form-vue').size() > 0) {
		var main = new Vue({
	        el: '#form-vue',
	        data:{
	        	isento:true,
		        cep:jQuery('input[name="billing_postcode"]').val(),
		        country:'BR',
				endereco: {
					logradouro:jQuery('input[name="billing_address_1"]').val(),
					number:jQuery('input[name="billing_number"]').val(),
					complemento:jQuery('input[name="billing_address_2"]').val(),
					bairro:jQuery('input[name="billing_neighborhood"]').val(),
					localidade:jQuery('input[name="billing_city"]').val(),				
					uf:jQuery('select[name="billing_state"] option:selected').val()
				},
				cep_shipping:jQuery('input[name="shipping_postcode"]').val(),
		        country_shipping:'BR',
				endereco_shipping: {
					logradouro:jQuery('input[name="shipping_address_1"]').val(),
					number:jQuery('input[name="shipping_number"]').val(),
					complemento:jQuery('input[name="shipping_address_2"]').val(),
					bairro:jQuery('input[name="shipping_neighborhood"]').val(),
					localidade:jQuery('input[name="shipping_city"]').val(),				
					uf:jQuery('select[name="shipping_state"] option:selected').val()
				}
			},
			watch:{
				cep: function(value){
					var self = this;
					v=value.replace(/D/g,"")                
					v=v.replace(/^(\d{5})(\d)/,"$1-$2") 
					self.cep = v
					if(/^[0-9]{5}-[0-9]{3}$/.test(self.cep)){
	    				jQuery.getJSON('https://viacep.com.br/ws/'+self.cep+'/json/',function(result){
							self.endereco = result;
							self.endereco.number = '';
							jQuery( document.body ).trigger( 'update_checkout' );
	    				});    				
	        		}

				},
				cep_shipping: function(value){
					var self = this;
					v=value.replace(/D/g,"")                
					v=v.replace(/^(\d{5})(\d)/,"$1-$2") 
					self.cep_shipping = v
					if(/^[0-9]{5}-[0-9]{3}$/.test(self.cep_shipping)){
	    				jQuery.getJSON('https://viacep.com.br/ws/'+self.cep_shipping+'/json/',function(result){
							self.endereco_shipping = result;
							self.endereco_shipping.number = '';
							jQuery( document.body ).trigger( 'update_checkout' );
	    				});    				
	        		}

				}
			}	
    	});
	}  
    
}(jQuery);