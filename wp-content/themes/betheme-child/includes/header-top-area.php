<?php $translate['wpml-no'] = mfn_opts_get('translate') ? mfn_opts_get('translate-wpml-no','No translations available for this page') : __('No translations available for this page','betheme'); ?>

<?php if( mfn_opts_get('action-bar')): ?>
	<div id="Action_bar">
		<div class="container">
			<div class="column one">
			
				<ul class="contact_details">
					<?php
						if( $header_slogan = mfn_opts_get( 'header-slogan' ) ){
							echo '<li class="slogan">'. $header_slogan .'</li>';
						}
						if( $header_phone = mfn_opts_get( 'header-phone' ) ){
							echo '<li class="phone"><i class="icon-phone"></i><a href="tel:'. str_replace(' ', '', $header_phone) .'">'. $header_phone .'</a></li>';
						}					
						if( $header_phone_2 = mfn_opts_get( 'header-phone-2' ) ){
							echo '<li class="phone"><i class="icon-phone"></i><a href="tel:'. str_replace(' ', '', $header_phone_2) .'">'. $header_phone_2 .'</a></li>';
						}					
						if( $header_email = mfn_opts_get( 'header-email' ) ){
							echo '<li class="mail"><i class="icon-mail-line"></i><a href="mailto:'. $header_email .'">'. $header_email .'</a></li>';
						}
					?>
				</ul>
				
				<?php 
					if( has_nav_menu( 'social-menu' ) ){
						mfn_wp_social_menu();
					} else {
						get_template_part( 'includes/include', 'social' );						
					}
				?>

			</div>
		</div>
	</div>
<?php endif; ?>

<?php 
	if( mfn_header_style( true ) == 'header-overlay' ){
		
		// Overlay ----------
		echo '<div id="Overlay">';
			mfn_wp_overlay_menu();
		echo '</div>';
		
		// Button ----------
		echo '<a class="overlay-menu-toggle" href="#">';
			echo '<i class="open icon-menu-fine"></i>';
			echo '<i class="close icon-cancel-fine"></i>';
		echo '</a>';
		
	}
?>

<!-- .header_placeholder 4sticky  -->
<div class="header_placeholder"></div>

<div id="Top_bar" class="loading">

	<div class="container">
		<div class="column one">
		
			<div class="top_bar_left clearfix">
			
				<!-- Logo -->
				<?php get_template_part( 'includes/include', 'logo' ); ?>
			
				<div class="menu_wrapper">
					<?php 
						if( ( mfn_header_style( true ) != 'header-overlay' ) && ( mfn_opts_get( 'menu-style' ) != 'hide' ) ){

							// #menu --------------------------
							if( in_array( mfn_header_style(), array( 'header-split', 'header-split header-semi', 'header-below header-split' ) ) ){
								mfn_wp_split_menu();
							} else {
								mfn_wp_nav_menu();
							}
						
							// responsive menu button ---------
							$mb_class = '';
							if( mfn_opts_get('header-menu-mobile-sticky') ) $mb_class .= ' is-sticky';

							echo '<a class="responsive-menu-toggle '. $mb_class .'" href="#">';
								if( $menu_text = trim( mfn_opts_get( 'header-menu-text' ) ) ){
									echo '<span>'. $menu_text .'</span>';
								} else {
									echo '<i class="icon-menu-fine"></i>';
								}  
							echo '</a>';
							
						}
					?>					
				</div>			
				
				<div class="secondary_menu_wrapper">
					<!-- #secondary-menu -->
					<?php mfn_wp_secondary_menu(); ?>
				</div>
				
				<div class="banner_wrapper">
					<?php mfn_opts_show( 'header-banner' ); ?>
				</div>
				
				<div class="search_wrapper">
					<!-- #searchform -->					
					<?php get_search_form( true ); ?>					
				</div>				
				<?php if ( is_user_logged_in() ) {
					echo '<a href="https://www.poopstore.com.br/minha-conta/" class="wb-btn-link-login">Olá, '.wp_get_current_user()->user_firstname . '<br><div class="link-text">Minha Conta</div></a>';
					}
				?>
				<?php if ( !is_user_logged_in() ) {

						echo '<a href="https://www.poopstore.com.br/minha-conta/" class="wb-btn-login"><i class="icon-login"></i></a>';
					}
				?>
				<a href="https://www.poopstore.com.br/lojista/" class="wb-btn-lojista"><i class="icon-shop-line"></i>Acesso Lojista</a>
			</div>
			
			<?php 
				if( ! mfn_opts_get( 'top-bar-right-hide' ) ){
					get_template_part( 'includes/header', 'top-bar-right' );
				}
			?>
			
		</div>
	</div>
	<div class="wb-menu-categories">
		<ul class="wb-list-menu">
			<!-- 			<li class="wb-li-category wb-category-dream">
				<a href="https://www.poopstore.com.br/dream/" class="wb-link-category">
					Dream
					<span class="wb-link-hover">
						<img src="https://www.poopstore.com.br/wp-content/uploads/2018/03/dropdown-category-dream.jpg">
					</span>
				</a>
			</li> -->
			<li class="wb-li-category wb-category-draw">
				<a href="https://www.poopstore.com.br/draw/" class="wb-link-category">
					Draw
					<span class="wb-link-hover">
						<img src="https://www.poopstore.com.br/wp-content/uploads/2018/03/dropdown-category-draw.jpg">
					</span>
				</a>
			</li>
			<li class="wb-li-category wb-category-copo">
				<a href="https://www.poopstore.com.br/copos/" class="wb-link-category">
					Copos
					<span class="wb-link-hover">
						<img src="https://www.poopstore.com.br/wp-content/uploads/2018/03/dropdown-category-copo.jpg">
					</span>
				</a>
			</li>
			<li class="wb-li-category wb-category-bonecos">
				<a href="https://www.poopstore.com.br/bonecos/" class="wb-link-category">
					Bonecos
					<span class="wb-link-hover">
						<img src="https://www.poopstore.com.br/wp-content/uploads/2018/03/dropdown-category-boneco.jpg">
					</span>
				</a>
			</li>
			<li class="wb-li-category wb-category-pantufas">
				<a href="https://www.poopstore.com.br/pantufas/" class="wb-link-category">
					Pantufas
					<span class="wb-link-hover">
						<img src="https://www.poopstore.com.br/wp-content/uploads/2018/03/dropdown-category-pantufa.jpg">
					</span>
				</a>
			</li>
			<li class="wb-li-category wb-category-almofadas">
				<a href="https://www.poopstore.com.br/emoji/" class="wb-link-category">
					Emoji
					<span class="wb-link-hover">
						<img src="https://www.poopstore.com.br/wp-content/uploads/2018/03/dropdown-category-almofada.jpg">
					</span>
				</a>
			</li>
		</ul>
	</div>	
</div>