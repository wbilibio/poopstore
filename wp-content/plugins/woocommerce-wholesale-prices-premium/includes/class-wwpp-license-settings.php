<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( !class_exists( 'WWPP_License_Settings' ) ) {

    /**
     * Model that houses the logic of plugin license settings page.
     *
     * @since 1.14.0
     */
    class WWPP_License_Settings {

        /*
        |--------------------------------------------------------------------------
        | Class Properties
        |--------------------------------------------------------------------------
        */

        /**
         * Property that holds the single main instance of WWPP_License_Settings.
         *
         * @since 1.14.0
         * @access private
         * @var WWPP_License_Settings
         */
        private static $_instance;




        /*
        |--------------------------------------------------------------------------
        | Class Methods
        |--------------------------------------------------------------------------
        */

        /**
         * WWPP_License_Settings constructor.
         *
         * @since 1.14.0
         * @since 1.16.1 Tidy up codebase. Remove injected dependency, its not being used anywhere here.
         * @access public
         *
         * @param array $dependencies Array of instance objects of all dependencies of WWPP_License_Settings model.
         */
        public function __construct() {}

        /**
         * Ensure that only one instance of WWPP_License_Settings is loaded or can be loaded (Singleton Pattern).
         *
         * @since 1.14.0
         * @since 1.16.1 Tidy up codebase. Remove injected dependency, its not being used anywhere here.
         * @access public
         *
         * @param array $dependencies Array of instance objects of all dependencies of WWPP_License_Settings model.
         * @return WWPP_License_Settings
         */
        public static function instance() {

            if ( !self::$_instance instanceof self )
                self::$_instance = new self();

            return self::$_instance;

        }

        /**
         * Load scripts related to wwpp license settings page.
         * 
         * @since 1.16.1
         * @access public
         */
        public function load_back_end_styles_and_scripts() {

            if ( isset( $_GET[ 'page' ] ) && $_GET[ 'page' ] == 'wwc_license_settings' &&
               ( ( isset( $_GET[ 'tab' ] ) && $_GET[ 'tab' ] == 'wwpp' ) || WWS_LICENSE_SETTINGS_DEFAULT_PLUGIN == 'wwpp' ) ) {
                // WWPP WSS License Settings Page

                $WWPP_PLUGIN_URL = plugins_url() . '/woocommerce-wholesale-prices-premium/';
                $WWPP_CSS_URL    = $WWPP_PLUGIN_URL . 'css/';
                $WWPP_JS_URL     = $WWPP_PLUGIN_URL . 'js/';

                wp_enqueue_style( 'wwpp_toastr_css' , $WWPP_JS_URL . 'lib/toastr/toastr.min.css' , array() , '1.16.1' , 'all');
                wp_enqueue_style( 'wwpp_wws_license_settings_css' , $WWPP_CSS_URL . 'wwpp-wws-license-settings.css' , array() , '1.16.1' , 'all');

                wp_enqueue_script( 'wwpp_toastr_js' , $WWPP_JS_URL . 'lib/toastr/toastr.min.js' , array( 'jquery' ) , 1 , true );
                wp_enqueue_script( 'wwpp_backEndAjaxServices_js' , $WWPP_JS_URL . 'app/modules/BackEndAjaxServices.js', array( 'jquery' ) , '1.16.1' , true );
                wp_enqueue_script( 'wwpp_wws_license_settings_js' , $WWPP_JS_URL . 'app/wwpp-wws-license-settings.js' , array( 'jquery' ) , '1.16.1' , true );

                wp_localize_script( 'wwpp_wws_license_settings_js' , 'wwpp_wws_license_settings_params' , array(
                    'i18n_success_save_wholesale_price' => __( 'Wholesale Prices License Details Successfully Saved' , 'woocommerce-wholesale-prices-premium' ),
                    'i18n_fail_save_wholesale_price'    => __( 'Failed To Save Wholesale Prices License Details' , 'woocommerce-wholesale-prices-premium' )
                ) );

            }

        }

        /**
         * Register general wws license settings page.
         *
         * @since 1.0.1
         * @since 1.14.0 Refactor codebase and move to its own model.
         * @access public
         */
        public function register_wws_license_settings_menu() {

            /*
            * Since we don't have a primary plugin to add this license settings, we have to check first if other plugins
            * belonging to the WWS plugin suite has already added a license settings page.
            */
            if ( !defined( 'WWS_LICENSE_SETTINGS_PAGE' ) ) {

                if ( !defined( 'WWS_LICENSE_SETTINGS_DEFAULT_PLUGIN' ) )
                    define( 'WWS_LICENSE_SETTINGS_DEFAULT_PLUGIN' , 'wwpp' );

                // Register WWS Settings Menu
                add_submenu_page(
                    'options-general.php', // Settings
                    __( 'WooCommerce Wholesale Suit License Settings' , 'woocommerce-wholesale-prices-premium' ),
                    __( 'WWS License' , 'woocommerce-wholesale-prices-premium' ),
                    apply_filters( 'wwpp_can_access_admin_menu_cap' , 'manage_options' ),
                    'wwc_license_settings',
                    array( $this , "wwc_general_license_settings_page" )
                );

                /*
                * We define this constant with the text domain of the plugin who added the settings page.
                */
                define( 'WWS_LICENSE_SETTINGS_PAGE' , 'woocommerce-wholesale-prices-premium' );

            }

        }

        /**
         * General WWS general license settings view.
         *
         * @since 1.0.2
         * @since 1.14.0 Refactor codebase and move to its own model.
         */
        public function wwc_general_license_settings_page() {

            ob_start();

            require_once ( plugin_dir_path( __DIR__ ) . "/views/wws-license-settings/wwpp-view-general-wws-settings-page.php" );

            echo ob_get_clean();

        }

        /**
         * WWPP WWS license settings header.
         *
         * @since 1.0.2
         * @since 1.14.0 Refactor codebase and move to its own model.
         */
        public function wwc_license_settings_header() {

            ob_start();

            if ( isset( $_GET[ 'tab' ] ) )
                $tab = $_GET[ 'tab' ];
            else
                $tab = WWS_LICENSE_SETTINGS_DEFAULT_PLUGIN;

            global $wp;
            $current_url = add_query_arg( $wp->query_string , '?' , home_url( $wp->request ) );
            $wwpp_license_settings_url = $current_url . "/wp-admin/options-general.php?page=wwc_license_settings&tab=wwpp"; ?>

            <a href="<?php echo $wwpp_license_settings_url; ?>" class="nav-tab <?php echo ( $tab == "wwpp" ) ? "nav-tab-active" : ""; ?>"><?php _e( 'Wholesale Prices' , 'woocommerce-wholesale-prices-premium' ); ?></a>

            <?php echo ob_get_clean();

        }

        /**
         * WWPP WWS license settings page content.
         *
         * @since 1.0.2
         * @since 1.14.0 Refactor codebase and move to its own model.
         */
        public function wwc_license_settings_page() {

            ob_start();

            require_once ( plugin_dir_path( __DIR__ ) . "/views/wws-license-settings/wwpp-view-wss-settings-page.php" );

            echo ob_get_clean();

        }

        /**
         * Check if in wwpp license settings page.
         *
         * @since 1.2.2
         * @since 1.14.0 Refactor codebase and move to its proper model.
         * @access public
         *
         * @return bool
         */
        public function check_if_in_wwpp_settings_page() {

            if ( isset( $_GET[ 'page' ] ) && $_GET[ 'page' ] == 'wwc_license_settings' && isset( $_GET[ 'tab' ] ) && $_GET[ 'tab' ] == 'wwpp' )
                return true;
            else
                return false;

        }

        /**
         * Save wwpp license details.
         *
         * @since 1.0.1
         * @since 1.14.0 Refator codebase and move to its proper model.
         *
         * @param null|array $license_details License details.
         * @return bool Operation status.
         */
        public function save_license_details( $license_details = null ) {

            if ( defined( 'DOING_AJAX' ) && DOING_AJAX )
                $license_details = $_POST[ 'licenseDetails' ];

            update_option( 'wwpp_option_license_email' , trim( $license_details[ 'license_email' ] ) );
            update_option( 'wwpp_option_license_key' , trim( $license_details[ 'license_key' ] ) );

            if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {

                @header( 'Content-Type: application/json; charset=' . get_option( 'blog_charset' ) );
                echo wp_json_encode( array( 'status' => 'success' ) );
                wp_die();

            } else
                return true;

        }




        /*
        |---------------------------------------------------------------------------------------------------------------
        | Execute model
        |---------------------------------------------------------------------------------------------------------------
        */
        
        /**
         * Register model ajax handlers.
         *
         * @since 1.14.0
         * @access public
         */
        public function register_ajax_handler() {

            add_action( "wp_ajax_wwppSaveLicenseDetails" , array( $this , 'save_license_details' ) );
            
        }

        /**
         * Execute model.
         *
         * @since 1.14.0
         * @since 1.16.1 Refactor codebase, add new version check codebase here.
         * @access public
         */
        public function run() {

            add_action( 'init'                             , array( $this , 'register_ajax_handler' ) );
            add_action( 'admin_enqueue_scripts'            , array( $this , 'load_back_end_styles_and_scripts' ) );
            add_action( "admin_menu"                       , array( $this , 'register_wws_license_settings_menu' ) );
            add_action( "wws_action_license_settings_tab"  , array( $this , 'wwc_license_settings_header' ) );
            add_action( "wws_action_license_settings_wwpp" , array( $this , 'wwc_license_settings_page' ) );

        }

    }

}