��                �      �     �  �     	  �  �  �     �     �     �  4     -   I     w     �  +   �  =   �     	     #	     A	     ]	  %   f	  �   �	  	   M
     W
     j
      z
     �
     �
     �
    �
  $   �       e  1     �  �   �  �  �  �  S     +     =     C  J   b  7   �     �     �  0     H   B  !   �  '   �  !   �     �  1     �   9     
            #   ,     P     d  %   v  P  �  ,   �        %1$s Price (%2$s) <b>WooCommerce Wholesale Prices</b> plugin missing dependency.<br/><br/>Please ensure you have the <a href="http://wordpress.org/plugins/woocommerce/" target="_blank">WooCommerce</a> plugin installed and activated.<br/> <p>Thanks for using our free WooCommerce Wholesale Prices plugin – we hope you are enjoying it so far.</p>
                                <p>We’d really appreciate it if you could take a few minutes to write a 5-star review of our Wholesale Prices plugin on WordPress.org!</p>
                                <p>Your comment will go a long way to helping us grow and giving new users the confidence to give us a try.</p>
                                <p>Thanks in advance, we are looking forward to reading it!</p> <p>We see you have been using Wholesale Suite for a couple of weeks now – thank you once again for your purchase and we hope you are enjoying it so far!</p>
                                         <p>We’d really appreciate it if you could take a few minutes to write a 5-star review of our Wholesale Prices plugin on WordPress.org!</p>
                                         <p>Your comment will go a long way to helping us grow and giving new users the confidence to give us a try.</p>
                                         <p>Thanks in advance, we are looking forward to reading it!</p>
                                         <p>PS. If you ever need support, please just <a href="%1$s" target="_blank">get in touch here.</a></p> Activate this plugin Auto Click here to activate &rarr; Click here to install from WordPress.org repo &rarr; Enter a value (leave blank to remove pricing) Important Notice: Invalid AJAX call Only applies to users with the role of %1$s Only applies to users with the role of %1$s for %2$s currency Required parameter not passed Review request response saved Set wholesale prices (%1$s) Settings This is the main wholesale user role. We have detected an outdated version of WooCommerce Wholesale Prices Premium. You require at least version 1.16.0 for this version of WooCommerce Wholesale Prices ( 1.6.0 ). Please update now. Wholesale Wholesale Customer Wholesale Price Wholesale Price for this product Wholesale Price: Wholesale Prices Wholesale Prices Settings Wholesale prices are set per role and currency.<br/><br/><b>Note:</b> Wholesale price must be set for the base currency to enable wholesale pricing for that role. The base currency will be used for conversion to other currencies that have no wholesale price explicitly set (Auto). WooCommerce Wholesale Prices Premium WooCommerce Wholesale Suite Project-Id-Version: WooCommerce Wholesale Prices
POT-Creation-Date: 2018-05-02 11:45+0800
PO-Revision-Date: 2018-06-06 06:20+0000
Language-Team: Português do Brasil
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-KeywordsList: __;_e;_n;_x
Last-Translator: William Bilibio <wbilibio@gmail.com>
Language: pt_BR
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: .hg
X-Poedit-SearchPathExcluded-1: .idea
Report-Msgid-Bugs-To:  %1$s Preço (%2$s) <b> Plugin dos preços de atacado de WooCommerce </ b> em falta da dependência. <br/> <br/> Certifiquese de que você tem o <a href = "http://wordpress.org/plugins/woocommerce/" target = "_ blank" > WooCommerce </a> plugin instalado e ativado. <br/> <p> Obrigado por usar nosso plug-in gratuito de preços de atacado WooCommerce - esperamos que você goste até agora. </ p>
                                 <p> Gostaríamos muito que você dedicasse alguns minutos para escrever uma resenha de 5 estrelas do nosso plug-in Preços por atacado no WordPress.org! </ p>
                                 <p> Seu comentário será de grande ajuda para nos ajudar a crescer e dar aos novos usuários a confiança para nos dar uma chance. </ p>
                                 <p> Agradecemos antecipadamente, estamos ansiosos para lê-lo! </ p> <p> Nós vemos que você tem usado o Wholesale Suite por algumas semanas - obrigado mais uma vez pela sua compra e esperamos que você esteja gostando disso até agora! </ p>
                                          <p> Gostaríamos muito que você dedicasse alguns minutos para escrever uma resenha de 5 estrelas do nosso plug-in Preços por atacado no WordPress.org! </ p>
                                          <p> Seu comentário será de grande ajuda para nos ajudar a crescer e dar aos novos usuários a confiança para nos dar uma chance. </ p>
                                          <p> Agradecemos antecipadamente, estamos ansiosos para lê-lo! </ p>
                                          <p> PS. Se você precisar de suporte, apenas <a href="%1$s" target="_blank"> entre em contato aqui. </a> </ p> Ative este plugin Auto
 Clique aqui para ativar &rarr; Clique aqui para instalar a partir do repositório do WordPress.org &rarr; Insira um valor (deixe em branco para remover o preço) Notícia importante: Chamada AJAX inválida Aplica-se apenas a usuários com o papel de %1$s Aplica-se apenas a utilizadores com o papel de %1$s para a moeda de %2$s Parâmetro requerido não passado Rever a resposta da solicitação salva Definir preços no atacado (%1$s) Configurações Este é o principal papel do usuário no atacado. Detectamos uma versão desatualizada do WooCommerce Wholesale Prices Premium. Você precisa de pelo menos a versão 1.16.0 para esta versão do WooCommerce Wholesale Prices (1.6.0). Por favor, atualize agora. Atacado Lojista Preço de atacado Preço de atacado para este produto Preço por atacado: Preço de atacado Configurações de preços no atacado Os preços de atacado são definidos por função e moeda. <br/> <br/> <b> Observação: </ b> O preço de atacado deve ser definido para a moeda base para permitir preços de atacado para essa função. A moeda base será usada para conversão para outras moedas que não tenham preço de atacado definido explicitamente (Automático). Prêmio de preços de atacado de WooCommerce Suíte Atacado WooCommerce 