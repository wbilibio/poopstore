<?php
/**
 * Proceed to checkout button
 *
 * Contains the markup for the proceed to checkout button on the cart.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/proceed-to-checkout-button.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<?php $user_info = get_userdata(get_current_user_id());
	global $woocommerce;
      $userRole = implode(',', $user_info->roles);
      $price = WC()->cart->get_cart_contents_total();
      if($userRole == 'wholesale_customer' && $price < 500){
		echo '<div class="woocommerce-error alert alert_error" role="alert">'.
				'<div class="alert_icon"><i class="icon-alert"></i></div>'.
				'<div class="alert_wrapper">O pedido minimo para lojista é de R$ 500,00. Adicione mais itens ao carrinho para finalizar seu orçamento.</div>'.
				'<a class="close" href="#"><i class="icon-cancel"></i></a>'.
			'</div>'.
			'<a href="" class="checkout-button button alt wc-forward no-active"><i class="icon-thumbs-up-line"></i> '.
				'Concluir orçamento'.
			'</a>';
		} elseif($userRole == 'wholesale_customer') {
			echo '<a href="'. esc_url(wc_get_checkout_url()) . '" class="checkout-button button alt wc-forward"><i class="icon-thumbs-up-line"></i> '.
					'Concluir orçamento'.
				'</a>';
		} else {
			echo '<a href="'. esc_url(wc_get_checkout_url()) . '" class="checkout-button button alt wc-forward"><i class="icon-thumbs-up-line"></i> '.
					'Concluir a compra'.
				'</a>';
		};
?> 

